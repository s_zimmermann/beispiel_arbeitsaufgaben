# Workflow Anfrage Datenanonymisierung und Datenarchivierung

| Thema | Information  | 
|----------|----------|
| Datensatz Name    |    |
| Eingereicht von    |    |
| Ablage    |    |

## Checkliste Datenannahme

### Prüfung: Dateinamen

- [ ] Prüfen: Keine Sonderzeichen, Umlaute oder Leerzeichen
- [ ] Prüfen: möglichst kurz
- [ ] Prüfen: Bezeichnung gibt Rückschluss auf Studie
	
### Prüfung: Dateiformat

- [ ] Prüfen: Liegt im Stata Format dta vor
- [ ] Prüfen: Datensatz ist in UTF-8 abgespeichert

### Prüfung: Variablennamen 

- [ ] Prüfen: einheitliches Namens-Schema
- [ ] Prüfen: kurze Namen
- [ ] Prüfen: Keine Umlaute oder Leerzeichen
- [ ] Prüfen: Unterstriche als Verbindungszeichen nutzen
- [ ] Prüfen: Variablennamen sollten immer klein geschrieben sein

### Prüfung: Variablen und Wertelbels 
- [ ] Prüfen: Variablenlabels für alle Variablen vorhanden
- [ ] Prüfen: Wertelabels für alle Variablen vorhanden
- [ ] Prüfen: Variablen- und Wertelabels aussagekräftig 
- [ ] Prüfen: Offene Angaben optimalerweise bereits codiert
- [ ] Prüfen: Keine Sonderzeichen und Umlaute
- [ ] Prüfen: Idealerweise Mehrsprachige Labels

### Prüfung: Fehlende Werte

- [ ] Prüfen: Fehlende Werte sind definiert
- [ ] Prüfen: Keine Systemmissings
- [ ] Prüfen: Idealerweise  entsprechen fehlende Werte dezim Panel

### Prüfung: Datenschutz und rechtliche Aspekte

- [ ] Prüfen: ID Variablen sind vorhanden und keine Namen
- [ ] Prüfen: Keine kleinteiligen regionalen Angaben
- [ ] Prüfen: Keine urheberrechtlichen oder vertraglichen Hindernisse

### Prüfung: Plausibilitäts- und Konsistenzprüfungen

- [ ] Prüfen: Filterführung stimmt
- [ ] Prüfen: Keine Werte außerhalb des zulässigen Wertebereichs 

### Prüfung: Datensatz anonymisieren

- [ ] Löschen: Direkte Identifikatoren: Namen, Adresse, E-Mail-Adresse, z.T. offene Angaben, etc. 
- [ ] Kategorisieren und/oder anonymisieren: Geburtsjahr, Länderangaben, Einkommen, Berufsangaben, etc.
- [ ] Kategorisieren und/oder anonymisieren: Gesundheit, politische Einstellung, religiöse Überzeugungen, ethnische Herkunft, etc.


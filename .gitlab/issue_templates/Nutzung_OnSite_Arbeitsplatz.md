# Workflow Nutzung OnSite Arbeitsplatz

| Thema | Information  | 
|----------|----------|
| Name    |     |
| Email Adresse    |   |
| Forschungsvorhaben    |    |
| Länge des Aufenthaltes    |  -  |
| Arbeitszeiten von Arbeitsplatz    |    |
| Importdateien    |  ja/nein  |
| zusätzliche R-Pakete    | ja/nein   |

## Checkliste Gastbetreuung Ablauf

### Kontaktaufnahme und Dokumentation (?)
- [ ] Beantragung OnSite Datensatz Controlcenter -> Email von Nutzenden an FDZ Adresse
- [ ] Issue mit Basisinformationen wird angelegt. guest_nachname
- [ ] schreibt Nutzungsinformationen in Issue
- [ ] Raum wird für Aufenthalt gebucht
- [ ] Empfang wird über Gastaufenthalt informiert

### Belehrung der Richtlinien (?)
- [ ] Versendung von Import und Exportregeln an Gastnutzenden und bitte Import Dateien per Plik zu senden
- [ ] Warten auf Bestätigung Gastaufenthalt von Nutzenden
- [ ] Nach Bestätigung Weiterleitung Issue an IT (Andi, Josha, Paul).

### Einrichtung Arbeitsplatz (IT (Andi, Josha, Paul)) 
- [ ] Updates OnSite Rechner?
- [ ] Anlegen Nutzender? Benutzerprofil/Passwort
- [ ] Anlegen Nutzerverzeichnis mit Import und Export Ordner
- [ ] Zugangspasswort wird an ? übermittelt
- [ ] IT prüft und lädt Importdateien PLIK und R Pakete für Nutzende auf Gastarbeitsplatz hoch.
- [ ] Nach Abschluss assign ?

### Übermittlung Zugangsdaten (?)
- [ ] ? versendet Zugangspasswort an Nutzenden und Bekanntgabe der Betreuungsperson
- [ ] ? schreibt alle Vereinbarungen (Ankunft, Aufenthalt ...) mit Nutzenden in Issue
- [ ] Assign an Betreuungsperson ?.

### Betreuung vor Ort (?) 
- [ ] Zugang zum Gebäude über Schlüssel/Responder
- [ ] Gästebucheintrag für jeden Tag der Anwesenheit
- [ ] ? nimmt Nutzende in Empfang und zeigt Gastarbeitsplatz
- [ ] Besondere Fragen und Probleme in Issues schreiben und beantworten (Fragen zu Export, Daten, IT ...)
- [ ] Bei Beendigung des Aufenthaltes Assign an Doreen

### Exportanfrage wird gestellt (?) 
- [ ] Gestellte Exportanfrage von Person wird ins Issue geschrieben
- [ ] Assign an verantwortliche Person (?) für Exportkontrolle 

### Exportkontrolle (?) 
- [ ] Export wird per Exportregeln überprüft. 
- [ ] Problem: Fehler wird an Doreen gemeldet, die Rückfrage an Nutzenden weiterleitet und Antwort ins Issue schreibt. 
- [ ] Kein Problem: Download Exportordner wird freigegeben und in PLIK vorbereitet

### Übermittlung Export-Dateien (?)
- [ ] Exportordner wird über Plik versendet an Person verschickt 	
	

  
  
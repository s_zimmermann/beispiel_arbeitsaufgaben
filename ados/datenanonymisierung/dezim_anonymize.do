* Ado zum Anonymisieren

*###############################################################################
* Programm zur partiellen Anonymisierung
capture program drop dezim_anonymize_partial	
program define dezim_anonymize_partial
	version 17.0
    syntax, varlist(string) cellminimum(int) missingcode(int)
	
    global variables_anonymized "`varlist'"
	
	display as error "Folgende Variablen werden anonymisiert:"
	display "`varlist'"
	
	quietly: foreach variable of global variables_anonymized {
		clonevar `variable'_anonymized = `variable'
		replace `variable'_anonymized = .
		bysort `variable': gen `variable'_flag = _N if `variable'>=-968
		replace `variable'_anonymized=`variable' if `variable'_flag>=`cellminimum'
		replace `variable'_anonymized = `missingcode' if `variable'_flag<`cellminimum'
		drop `variable'_flag
		label define `variable' `missingcode' "Anonymisiert", add
		drop `variable'
		rename `variable'_anonymized `variable'
	}
	
	display as error "Variablen wurden auf `missingcode' recodiert (`missingcode' Anonymisiert), wenn die Anzahl an Observationen kleiner `cellminimum' ist"
end

*###############################################################################
* Programm zur kompletten Anonymisierung
capture program drop dezim_anonymize_complete
program define dezim_anonymize_complete
	version 17.0
    syntax, varlist(string) missingcode(int)
	
    global variables_anonymized "`varlist'"
	
	display as error "Folgende Variablen werden vollständig anonymisiert:"
	display "`varlist'"
	
	quietly: foreach variable of global variables_anonymized {
		clonevar `variable'_anonymized = `variable'
		replace `variable'_anonymized = `missingcode'
		label define `variable'_anonymized `missingcode'  "Anonymisiert", add
		drop `variable'
		rename `variable'_anonymized `variable'
	}
	
	display as error "Variablen wurden auf `missingcode' recodiert (`missingcode' Anonymisiert)."
end

*###############################################################################
* Programm um alle Variablennamen kleinzuschreiben
capture program drop dezim_lowercase_variables
program define dezim_lowercase_variables
	version 17.0
		
	quietly: foreach variable of varlist _all {
					rename `variable' `=strlower("`variable'")'
	}
		
    display as error "Variablen wurden auf lowercase gesetzt"
end

*###############################################################################
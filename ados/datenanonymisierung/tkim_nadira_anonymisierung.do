*** Anonymisierung "CATI_DATA_mitUmlauten.dta"

* Globals Definieren
global dataset = "C:\Users\zimmermann\Downloads\CATI_DATA_mitUmlauten.dta"
global dataset_anonymisiert = "C:\Users\zimmermann\Downloads\CATI_DATA_mitUmlauten_anonymisiert.dta"
global dezim_anonymize_ado = "A:\git\beispiel_arbeitsaufgaben\ados\datenanonymisierung\dezim_anonymize.do"
global meta_output = "C:\Users\zimmermann\OneDrive - DeZIM-Institut e.V\Dokumente\Einarbeitung\"
global varlist "gk3 fbula soz2 soz7b soz8a soz8b soz8c soz8d soz8e soz9a soz9b soz10a soz10b soz11 soz12a soz19b Alter_num soz10ab soz10_reporting soz1 soz3 soz4 soz5 soz6 soz7c soz19a cob02a cob03a cob04a cob05a cob05c cob06a cob07a soz20_1 soz20_2 soz20_3 soz20_4 soz20_5 soz20_6 soz20_7 soz20_8 soz21_2 soz21_3 soz21_4 soz21_5 soz21_6 soz21_7 soz21_8"
global varlist_categorize "soz1_g soz3_g soz4_g soz5_g soz6_g soz7c_g soz19a_g"
global cellminimum = 50

do "$dezim_anonymize_ado"

* ##################### 1 Verarbeitung offener Angaben #########################
/* ###########################################################################*/

useold "$dataset", clear
ds, has(type string) 
global strvars "`r(varlist)'"
keep $strvars

describe, replace clear
keep name varlab
rename name variable
rename varlab label_de
gen anpassung = "gelöschte offene Angabe"
tempfile string_metadata
save `string_metadata'

useold "$dataset", clear
drop $strvars

* ##################### 2 Demografie Fragen Kategorisieren #####################
/* ###########################################################################*/

* Alter altersgruppen_g systemmissing zu -999 und Alter_kat löschen

* Bundesland
clonevar fbula_g = fbula
recode fbula_g (1/3 5/7 9 10 11 12 15 16= 2) (4 8 13 14 16=1)

label define fbula_g 1 "Ost" ///
					 2 "West" ///
					 -996 "Anonymisiert" /// 
					 -998 "Weiß nicht" /// 
					 -999 "keine Angabe/verweigert"
					 
label values fbula_g fbula_g

* Geburtsjahr
clonevar soz1_g = soz1
recode soz1_g (0/1997=1) (1998/2023=2)

label define soz1_g 1 "1997 und davor" /// 
					2 "1998 und danach" /// 
					-996 "Anonymisiert" /// 
					-998 "Weiß nicht" /// 
					-999 "keine Angabe/verweigert"
					
label values soz1_g soz1_g

* Haushaltsgröße
clonevar soz3_g = soz3
recode soz3_g (6/90=6)

label define soz3_g 6 "6 oder mehr" ///
				   -996 "Anonymisiert" /// 
				   -998 "Weiß nicht" /// 
				   -999 "keine Angabe/verweigert"
				   
label values soz3_g soz3_g

* Haushaltsgröße mind 14
clonevar soz4_g = soz4
recode soz4_g (6/90=6)

label define soz4_g 6 "6 oder mehr" /// 
				    -996 "Anonymisiert" /// 
					-998 "Weiß nicht" ///
					-999 "keine Angabe/verweigert"
					
label values soz4_g soz4_g

* Anzahl Festnetznummern
clonevar soz5_g = soz5
recode soz5_g (5/90=5)

label define soz5_g 5 "5 oder mehr" /// 
					-996 "Anonymisiert" ///
					-998 "Weiß nicht" /// 
					-999 "keine Angabe/verweigert"
					
label values soz5_g soz5_g

* Anzahl Handys
clonevar soz6_g = soz6
recode soz6_g (4/90=4)

label define soz6_g 4 "4 oder mehr" /// 
					  -996 "Anonymisiert" ///
					  -998 "Weiß nicht" /// 
					  -999 "keine Angabe/verweigert"
					  
label values soz6_g soz6_g

* In welchem Jahr sind Sie nach Deutschland gekommen?
clonevar soz7c_g = soz7c
recode soz7c_g (0/1997=1) (1998/2023=2)

label define soz7c_g 1 "1997 und davor" /// 
					 2 "1998 und danach" ///
					 -971 "Filter: Fragebogendramaturgie" /// 
					 -996 "Anonymisiert" ///
					 -998 "Weiß nicht" /// 
					 -999 "keine Angabe/verweigert"
					 
label values soz7c_g soz7c_g

* Nettoeinkommen
clonevar soz19a_g = soz19a
recode soz19a_g (0/749=1) ///
				(750/1499=2) ///
				(1500/1999=3) ///
				(2000/2499 = 4) ///
				(2500/2999 = 5) ///
				(3000/3999 = 6) ///
				(4000/4999 = 7) /// 
				(5000/7999 = 8) /// 
				(8000/100000000 = 9)

label define soz19a_g 1 "bis unter 750 Euro" /// 
					  2 "750 bis unter 1.500 Euro" ///
					  3 "1.500 bis unter 2.000 Euro" ///
					  4 "2.000 bis unter 2.500 Euro" /// 
					  5 "2.500 bis unter 3.000 Euro" /// 
					  6 "3.000 bis unter 4.000 Euro" /// 
					  7 "4.000 bis unter 5.000 Euro" /// 
					  8 "5.000 bis unter 8.000 Euro" /// 
					  9 "8.000 Euro und mehr" /// 
					  -971 "Filter: Fragebogendramaturgie" /// 
					  -996 "Anonymisiert" -998 "Weiß nicht" /// 
					  -999 "keine Angabe/verweigert"
					  
label values soz19a_g soz19a_g

/* ###########################################################################*/
* ##################### 3 Demografie Anonymisieren #############################

dezim_anonymize_partial, varlist("$varlist") /// 
						 cellminimum($cellminimum) /// 
						 missingcode (-996)
						 
dezim_anonymize_complete, varlist("soz20_2 soz20_4 soz20_8") ///
						  missingcode (-996)
						  
dezim_lowercase_variables						  
						  
* Offene Fragen?

* Variablen doppelt Unklare Bedeutung?
* des altersgruppen_g alter selbstident_g  index eng_index_g soz2_g

* Bedeutung nachfragen: selbstident_g index eng_index_g

* Variablen ohne Variablenlabel:
* A_recno A_datum A_Startzeit A_Endzeit Int_ID Auswahlwahrscheinlichkeit _merge soz2_g random alter altersgruppen_g soz10a_g eng1_d eng2_d eng3_d eng4_d antira_ebenen_g soz2_g fbula1
* Groß und Kleinschreibung Variablen sollten klein sein
* ABL_NBL Alter_kat 

* zu löschen? Geschlecht doppelt???
drop alter altersgruppen_g  alter_kat _merge soz2_g fbula1 abl_nbl

/* ###########################################################################*/
* ##################### 4 Datensatz sortieren ##################################

global ids = "a_recno int_id"
global paradaten = "antira_ebenen_g numtyp isstop fbula_g fbula random gk3 a_datum a_startzeit a_endzeit"
global jw = "jwb1 jwb2 jwb3 jwb4 jwb5 jwb6"
global bew = "bew_g bew1_g bew1 bew2_g bew2 bew3_g bew3 bew4_g bew4 bew5_g bew5 bew6_g bew6"
global omt =  "omt1_g omt2_g omt3_g omt4_g omt5_g omt6_g omt7_g omt8_g omt_rf1_g omt_rf2_g omt_rf3_g omt_rf4_g omt_rf5_g omt_rf6_g omt_rf7_g omt_rf8_g omt1_ge omt1_gr omt1_ber omt1 omt1_q omt2_ge omt2_gr omt2_ber omt2 omt2_q omt3_ge omt3_gr omt3_ber omt3 omt3_q omt4_ge omt4_gr omt4_ber omt4 omt4_q omt5_ge omt5_gr omt5_ber omt5 omt5_q omt6_ge omt6_gr omt6_ber omt6 omt6_q omt7_ge omt7_gr omt7_ber omt7 omt7_q omt8_ge omt8_gr omt8_ber omt8 omt8_q"
global ress = "ress1 ress2 ress3 ras1_g ras2_g ras3_g ras4_g ras5_g ras6_g ras7_g ras8_g ras9_g ras10_g ras11_g ras12_g ras13_g ras14_g ras15_g ras16_g ras17_g ras18_g ras19_g ras20_g ras21_g ras22_g ras1 ras2 ras3 ras4 ras5 ras6 ras7 ras8 ras9 ras10 ras11 ras12 ras13 ras14 ras15 ras16 ras17 ras18 ras19 ras20 ras21 ras22 erzaehlt_g venn_g"
global afe = "afe1_g afe2_g afe3_g afe4_g afe5_g afe6_g afe7_g afe1 afe2 afe3 afe4 afe5 afe6 afe7"
global sdo =  "sdo1 sdo2 sdo3"
global eng = "eng1 eng1_d eng2 eng2_d eng3 eng3_d eng4 eng4_d"
global cob = "cob1a cob1b cob1d cob1e cob02a cob02b cob02c cob02d cob02e cob03a cob03b cob03c cob03d cob03e cob04a cob04b cob04c cob04d cob04e cob05a cob05b cob05c cob05d cob05e cob06a cob06b cob06c cob06d cob06e cob07a cob07b cob07c cob07d cob07e cob08a cob08b cob09a cob09b cob09c cob09d"
global demografie = "soz00 soz1_g soz1 soz2 soz3_g soz3 soz4_g soz4 soz5_g soz5 soz6_g soz6 soz7a soz7b soz7c soz7c_g soz8a soz8b soz8c soz8d soz8e soz9a soz9b soz10a soz10b soz10ab soz10a_g soz10_reporting soz11 soz12a soz12a1 soz12c soz19a_g soz19a soz19b soz20_1 soz20_2 soz20_3 soz20_4 soz20_5 soz20_6 soz20_7 soz20_8 soz21_1 soz21_2 soz21_3 soz21_4 soz21_5 soz21_6 soz21_7 soz21_8 soz12a_reporting selbstident_g alter_num"
global cor = "cor1 cor2"
global ver = "ver1 ver2 ver3 ver4"
global ins = "ins_g ins1a_g ins2a_g ins3a_g ins4a_g ins5a_g ins1a ins2a ins3a ins4a ins5a ins1b_g ins2b_g ins3b_g ins4b_g ins5b_g ins6b_g ins1b ins2b ins3b ins4b ins5b ins6b"
global par = "pol3 par1 par2a_g par2b_g par2c_g par2d_g par2e_g par2f_g par2a par2b par2c par2d par2e par2f"
global anm = "anm1 anm2 anm3"
global gewichte = "auswahlwahrscheinlichkeit gew"
global ktk = "ktk_fam_g ktk_freundeskreis_g ktk_arbeit_g ktk_gesamt_g"
global index = "index eng_index_g"

order $ids $gewichte $demografie $jw $bew $omt $ress $afe $sdo $eng $cob $cor $ver $ins /// 
$par $anm $ktk $index $paradaten

save "$dataset_anonymisiert", replace

/* ###########################################################################*/
* ##################### 5 Metadaten Veränderungen ##############################
* 
describe, replace clear
keep name varlab
rename name variable
rename varlab label_de
gen anpassung = ""

foreach variable of global varlist {
	replace anpassung = "anonymisiert" if variable == "`variable'"
}

foreach variable of global varlist_categorize {
	replace anpassung = "anonymisiert und neu kategorisiert" if variable == "`variable'"
}

replace anpassung = "keine Anpassung" if anpassung == ""
append using `string_metadata'

export delimited using "$meta_output\veraenderungs_doku.csv", replace